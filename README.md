# PPK-Praktikum 13 : AsyncTask

## Identitas

```
Nama : Gilang W Prasetyo
NIM  : 222011750
Kelas: 3SI3

```

## Deskripsi

Asynctask ini digunakan untuk memanggil proses yang dijalankan secara background thread yang mana hasil dari proses ini akan mengupdate suatu nilai ada thread User Intreface (UI). Intinya selama ini kita menggunakan hanya satu thread untuk user interface dan proses background, dan ini dapat membuat Aplikasi kita seakan-akan freeze. Dengan Asynchtask proses background dikerjakan dengan thread yang terpisah. 

Asynctask ketika dijalankan akan menjalani 4 proses yaitu 

onPreExecute() dipanggil sebelum proses doInbackground dijalankan. Biasanya disini kita bisa memunculkan progressbar. 

doInbackground(Params…) dipanggil setelah code yang ada pada method onPreExecutte dijalankan. Disinilah biasanya proses yang memakan waktu lama dijalankan seperti permintaan data ke server, komputasi yang membutuhkan waktu lama.
 
onProgressUpdate(Progress…) dipanggil jika pada doInBackground ada yang memanggil method publishProgress. Biasanya bisa kita gunakan untuk mengetahui progress download dari sebuah file. 

onPostExecute(Result…) dipanggil setelah proses doinBackground selesai, method ini akan membaca hasil proses dari doInBackground yang digunakan untuk mengupdate nilai pada user interface. Jangan lupa menambahkan code untuk menutup progressbar.


## Praktikum Latihan AsyncTask
![cd_catalog.xml](https://gitlab.com/matkulpepeka/ppk-praktikum-13/-/tree/master/Screenshot/asyncTask1.png)
![cd_catalog.xml](https://gitlab.com/matkulpepeka/ppk-praktikum-13/-/tree/master/Screenshot/asyncTask2.png)
![cd_catalog.xml](https://gitlab.com/matkulpepeka/ppk-praktikum-13/-/tree/master/Screenshot/asyncTask3
## Penugasan Kelas AsyncTask telah terdeprecated sehingga disarankan menggunakan java.util.concurrent.
### AsyncTask Deprecated
![cd_catalog.xml](https://gitlab.com/matkulpepeka/ppk-praktikum-13/-/tree/master/Screenshot/asyncTaskDeprecated.png)
### Kode AsyncTask
```
package com.example.asynctasklatihan;
import androidx.appcompat.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    URL ImageUrl = null;
    InputStream is = null;
    Bitmap bmImg = null;
    ImageView imageView = null;
    Button button = null;
    ProgressDialog p;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button=findViewById(R.id.asyncTask);
        imageView = findViewById(R.id.image);
        button.setOnClickListener(new 
            View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    AsyncTaskExample asyncTask=new AsyncTaskExample();
                    asyncTask.execute("https://stis.ac.id/media/source/up.png");
                }
            }
        });
    }

    private class AsyncTaskExample extends 
        AsyncTask<String, String, Bitmap> {
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                p = new ProgressDialog(MainActivity.this);
                p.setMessage("Downloading...");
                p.setIndeterminate(false);
                p.setCancelable(false);
                p.show();
            }

            @Override
            protected Bitmap doInBackground(String... strings) {
                try {
                    ImageUrl = new URL(strings[0]);
                    HttpURLConnection conn = (HttpURLConnection) ImageUrl.openConnection();
                    conn.setDoInput(true);
                    conn.connect();
                    is = conn.getInputStream();
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                    bmImg = BitmapFactory.decodeStream(is, null, options);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    
                    return bmImg;
                }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                if(imageView!=null) {
                    p.hide();
                    imageView.setImageBitmap(bitmap);
                } else {
                    p.show();
                }
            }
        }
}
```

### Kode Executors (java.util.concurrent)
```
package com.example.asynctaskpractice;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {
    URL ImageUrl = null;
    Bitmap bmImg = null;
    ImageView imageView = null;
    Button button = null;
    ProgressDialog p;
    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button=findViewById(R.id.asyncTask);
        imageView = findViewById(R.id.image);
        button.setOnClickListener(view -> {
            ExecutorService service = Executors.newSingleThreadExecutor();
            service.execute(() -> {
                runOnUiThread(() -> {
                    p = new ProgressDialog(MainActivity.this);
                    p.setMessage("Downloading...");
                    p.setIndeterminate(false);
                    p.setCancelable(false);
                    p.show();
                });

                try {
                    ImageUrl = new URL("https://stis.ac.id/media/source/up.png");
                    HttpURLConnection httpURLConnection = (HttpURLConnection) ImageUrl.openConnection();
                    httpURLConnection.connect();
                    bmImg = BitmapFactory.decodeStream(httpURLConnection.getInputStream());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                runOnUiThread(() -> {
                    p.dismiss();
                    imageView.setImageBitmap(bmImg);
                });
            });
        });

    }
}
```

### Running Kode Executors (java.util.concurrent)
![cd_catalog.xml](https://gitlab.com/matkulpepeka/ppk-praktikum-13/-/tree/master/Screenshot/concurrentExecutors1.png)
![cd_catalog.xml](https://gitlab.com/matkulpepeka/ppk-praktikum-13/-/tree/master/Screenshot/concurrentExecutors2.png)
![cd_catalog.xml](https://gitlab.com/matkulpepeka/ppk-praktikum-13/-/tree/master/Screenshot/concurrentExecutors3.png)
